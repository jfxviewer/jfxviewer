module org.egedede.jfxviewer {
  requires javafx.controls;

  opens org.egedede.jfxviewer;
  opens org.egedede.jfxviewer.fileexplorer;
}