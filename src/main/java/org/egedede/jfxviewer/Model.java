package org.egedede.jfxviewer;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableObjectValue;

import java.nio.file.Path;

public class Model {

  public ObjectProperty<Path> path = new SimpleObjectProperty<Path>();

}
