package org.egedede.jfxviewer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;
import org.egedede.jfxviewer.fileexplorer.JfxFileExplorer;
import org.egedede.jfxviewer.viewer.TextViewer;

public class JfxViewer extends Application
{
    @Override
    public void start(Stage primaryStage) {
        Model  model = new Model();
        SplitPane pane = new SplitPane(new JfxFileExplorer(model), new TextViewer(model));
        Scene scene = new Scene(pane, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
