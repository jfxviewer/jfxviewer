package org.egedede.jfxviewer.viewer;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.egedede.jfxviewer.Model;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.stream.Collectors;

public class TextViewer extends VBox {

  private TextArea area = new TextArea();

  public TextViewer(Model model) {
    super(10);
    ScrollPane child = new ScrollPane(area);
    VBox.setVgrow(area, Priority.ALWAYS);
    getChildren().add(area);
    model.path.addListener(
        (observableValue, oldValue, newValue) -> {
          try {
            area.setText(new String(Files.readAllBytes(newValue)));
          } catch (IOException e) {
            area.setText(
                "Failed to read "
                    + newValue
                    + "\n"
                    + Arrays.stream(e.getStackTrace())
                        .map(st -> st.getClassName() + st.getMethodName())
                        .collect(Collectors.joining("\n\t")));
          }
        });
  }
}
