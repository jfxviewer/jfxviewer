package org.egedede.jfxviewer.fileexplorer;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import org.egedede.jfxviewer.Model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PathTreeCell extends TreeCell<Path> {
  private TextField textField;
  private Model model;

  public PathTreeCell(Model model) {
    this.model = model;
    setOnMouseClicked(
        ev -> {
          if (ev.getButton() == MouseButton.PRIMARY && ev.getClickCount() >= 2) {
            model.path.set(getTreeItem().getValue());
          }
        });
  }

  @Override
  public void startEdit() {
    super.startEdit();

    if (textField == null) {
      createTextField();
    }
    setText(null);
    setGraphic(textField);
    textField.selectAll();
  }

  @Override
  public void cancelEdit() {
    super.cancelEdit();
    setText( getItem().getFileName().toString());
    setTooltip(new Tooltip(buildTooltip(getItem())));
    setGraphic(getTreeItem().getGraphic());
  }

  private String buildTooltip(Path item) {
    StringBuilder builder = null;
    try {
      builder = new StringBuilder("type : ")
          .append(Files.probeContentType(item))
          .append("\n size ")
          .append(Files.size(item));
    } catch (IOException e) {
      e.printStackTrace();
      builder.append("Failed to load files information");
    }
    return builder.toString();
  }

  @Override
  public void updateItem(Path item, boolean empty) {
    super.updateItem(item, empty);

    if (empty) {
      setText(null);
      setGraphic(null);
    } else {
      if (isEditing()) {
        if (textField != null) {
          textField.setText(getString());
        }
        setText(null);
        setGraphic(textField);
      } else {
        setText(getString());
        setGraphic(getTreeItem().getGraphic());
      }
    }
  }

  private void createTextField() {
    textField = new TextField(getString());
    textField.setOnKeyReleased(t -> {
      if (t.getCode() == KeyCode.ENTER) {
//          commitEdit(textField.getText());
      } else if (t.getCode() == KeyCode.ESCAPE) {
        cancelEdit();
      }
    });
  }

  private String getString() {
    return getItem() == null ? "" : getItem().getFileName().toString();
  }
}
