package org.egedede.jfxviewer.fileexplorer;

import javafx.scene.control.TreeView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.egedede.jfxviewer.Model;

import java.nio.file.Path;

public class JfxFileExplorer extends VBox {

  public JfxFileExplorer(Model model) {
    super(10);
    TreeView<Path> child = new TreeView<>(new FileTreeItem(Path.of("/home/tiaped")));
    VBox.setVgrow(child, Priority.ALWAYS);
    getChildren().add(child);
    child.setCellFactory(param -> new PathTreeCell(model));
//    child.setOnMouseClicked(ev -> {
//      if(ev.isPrimaryButtonDown() && ev.getClickCount()>=2) {
//        model.path.set(child.getSelectionModel().getSelectedItem().getValue());
//      }
//    });
  }
}
