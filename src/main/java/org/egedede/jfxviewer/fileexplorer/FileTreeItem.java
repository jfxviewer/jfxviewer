package org.egedede.jfxviewer.fileexplorer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class FileTreeItem extends TreeItem<Path> {

  private final Path path;
  private boolean isLeaf;
  // We do the children and leaf testing only once, and then set these
  // booleans to false so that we do not check again during this
  // run. A more complete implementation may need to handle more
  // dynamic file system situations (such as where a folder has files
  // added after the TreeView is shown). Again, this is left as an
  // exercise for the reader.
  private boolean isFirstTimeChildren = true;
  private boolean isFirstTimeLeaf = true;

  public FileTreeItem(Path path) {
    super(path);
    this.path = path;
  }

  @Override
  public ObservableList<TreeItem<Path>> getChildren() {
    if (isFirstTimeChildren) {
      isFirstTimeChildren = false;

      // First getChildren() call, so we actually go off and
      // determine the children of the File contained in this TreeItem.
      try {
        super.getChildren().setAll(buildChildren(this));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return super.getChildren();
  }

  @Override
  public boolean isLeaf() {
    if (isFirstTimeLeaf) {
      isFirstTimeLeaf = false;
      Path f = getValue();
      isLeaf = Files.isRegularFile(f);
    }

    return isLeaf;
  }

  private ObservableList<TreeItem<Path>> buildChildren(FileTreeItem TreeItem) throws IOException {
    Path f = TreeItem.getValue();
    if (f != null && Files.isDirectory(f)) {
      Stream<Path> files = Files.list(f);
      if (files != null) {
        ObservableList<TreeItem<Path>> children = FXCollections.observableArrayList();

          files.forEach(p -> children.add(new FileTreeItem(p)));
        return children;
      }
    }

    return FXCollections.emptyObservableList();
  }

}
